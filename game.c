#include<stdio.h>

void set_individual(int row, int col, int val);
void print_arr();
void set_arr_to(int num);
void set_col_to(int col, int val);
void set_row_to(int row, int val);
void arrest(int x, int y);
int count_arrest();

int arr[8][8]; //[0-7][0-7]
int helicopters;

int main(){
	set_arr_to(1);
	printf("Each house in the matrix has a terrorist...\n");
	print_arr();
	printf("How many choppers to land ? ");
	scanf("%d", &helicopters);
	while(helicopters > 0){
		int x, y;
		printf("Enter the coordiantes to land the chopper [x,y = 1-8] #%d (x,y): ", helicopters);
		scanf("%d,%d", &x, &y);
		x--; y--;
		if(x <= 7 && y <= 7 && x >=0 && y >=0) { //0-7
			arrest(x, y);
			print_arr();
			printf("%d terrorists has been arrested by landing at %d, %d...\n", count_arrest(), x, y);
			helicopters = helicopters - 1;
		} else {
			printf("Wrong coordiantes, try again... \n");
		}
	}
	printf("------------------------------------------------------------------\n\n");
	printf("%d terrorists has been arrested totally...hail Indian Army... :D\n", count_arrest());
	return 0;

}

void arrest(int x, int y){
	set_col_to(y, 0);
	set_row_to(x, 0);
}

int count_arrest(){
	int count = 0;
	for(int i=0; i<8; i++){
		for(int j=0; j<8; j++){
			 if(arr[i][j] == 0){
			 	count++;
			 }
		}
	}
	return count;
}

void noop(){}

void print_arr(){
	printf("------------------------------\n");
	for(int i=0; i<8; i++){
		for (int j=0; j<8; j++){
			printf("%d  ", arr[i][j]);
		}
		printf("\n");
	}
	printf("------------------------------\n");
}

void set_arr_to(int num){
	for(int i=0; i<8; i++){
		for(int j=0; j<8; j++){
			arr[i][j] = num;
		}
	}
}

void set_col_to(int col, int val){
	for(int i=0; i<8; i++){
		arr[i][col] = val;
	}
}

void set_row_to(int row, int val){
	for(int i=0; i<8; i++){
		arr[row][i] = val;
	}
}

void set_individual(int row, int col, int val){
	arr[row][col] = val;
}